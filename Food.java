public class Food
{
    private String description;
    
    private int Calories;
    
    public int getCalories()
    {
        return Calories;
        
    }
    
    public setCalories (getCalories)
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    /**
     * set the description
     * @param
     */
     
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Override
    public String toString()
    {
        return "Somebody brought " + getDescription();
    }
    
}